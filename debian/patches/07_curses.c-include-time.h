Author: Brian Brazil <bbrazil@netsoc.tcd.ie>	vim:ft=diff:
Description: #include <time.h> to avoid segfault on amd64, BTS #345305

Index: b/curses.c
===================================================================
--- a/curses.c
+++ b/curses.c
@@ -20,6 +20,7 @@
  */
 
 #include "netris.h"
+#include <time.h>
 #include <sys/types.h>
 #include <unistd.h>
 #include <term.h>
