Author: David Mosberger <David.Mosberger@acm.org>	vim:ft=diff:
Description: #include <term.h> to avoid implicit pointer conversion, BTS #325926

Index: b/curses.c
===================================================================
--- a/curses.c
+++ b/curses.c
@@ -22,6 +22,7 @@
 #include "netris.h"
 #include <sys/types.h>
 #include <unistd.h>
+#include <term.h>
 #include <curses.h>
 #include <string.h>
 #include <stdlib.h>
