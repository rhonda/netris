netris (0.52-11) unstable; urgency=low

  [ Rhonda D'Vine ]
  * Bump Standards-Version to 3.9.8.
  * Remove homepage URL, it's down since 4 years now.
  * Drop watch file, upstream isn't there anymore.
  * Move VCS to salsa.
  * Bump Standards-Version to 4.6.1:
    - Bump debhelper-compat to 13.
  * Use Rules-Requires-Root: no.
  * Convert debian/copyright to DEP5.

  [ Vagrant Cascadian ]
  * debian/rules: Pass -ffile-prefix-map via CEXTRA in dh_auto_build override
    (closes: #1020657)

  [ Helmut Grohne ]
  * Fix FTCBFS: Pass a cross compiler to ./Configure. (closes: #883884)

 -- Rhonda D'Vine <rhonda@debian.org>  Mon, 26 Sep 2022 13:54:16 +0200

netris (0.52-10) unstable; urgency=medium

  * Fix bug number in former changelog entry for documentation purposes.
  * Fix Vcs-* fields.
  * Switch to source format 3.0 (quilt).
  * Change to dh style debian/rules.
  * Bump Standards-Version to 3.9.6.

 -- Rhonda D'Vine <rhonda@debian.org>  Sat, 02 Jan 2016 16:52:31 +0100

netris (0.52-9) unstable; urgency=low

  * Add Vcs-* fields.
  * Bump Standards-Version to 3.9.1.
  * Relicense packaging under WTFPLv2.
  * New patch fix-memory-leak to fix a memory leak and adding another memory
    related check. Thanks to TomaszN (closes: #590942)

 -- Gerfried Fuchs <rhonda@debian.at>  Fri, 13 Aug 2010 23:12:17 +0200

netris (0.52-8) unstable; urgency=low

  * The "once every two years" release.
  * Applied patch from Mats Erik Andersson for supporting ipv6
    (closes: #561977)
  * Added at least Description header into all patches.
  * Fix spelling error noticed by lintian in changelog.
  * Add debian/README.source referencing quilt's file.
  * Update Standards-Version to 3.8.4.
  * Update DEB_BUILD_OPTIONS handling according to policy.
  * Switched packaging licensing to BSD style.
  * set -e in post{rm,inst} instead of passing -e on hashbang line.

 -- Gerfried Fuchs <rhonda@debian.at>  Thu, 04 Mar 2010 21:36:09 +0100

netris (0.52-7) unstable; urgency=low

  * The "once every release" release.
  * Moved patches to quilt, split them into:
    - multi-games-with-scoring: allow multiple games in a row without
      restarting.
    - line-count-patch: patch to display cleared line counts
    - staircase-effect-fix: fix displaying of error messages
    - robot-close-fixup: Small fix for CloseRobot function to close only if a
      robot is used.
    - init-static-vars: initialize lostConn and gotEndConn static var in
      InitNet function.
    - curses.c-include-term.h: patch to fix function call that otherwise use
      implicit pointer conversion.
    - curses.c-include-time.h: patch to fix segfault on amd64.
  * New patches:
    - various-fixes: various small changes to fix compile warnings
  * Linked to GPL-2 in copyright file directly, added hint for later versions.
  * Added copyright information into the debianization scripts.
  * Added Homepage: control field.
  * Updated package to Standards-Version 3.7.3, updating menu section for that.
  * Don't ignore make clean errors anymore.
  * Small cosmetic updates to the netris-sample-robot manpage.

 -- Gerfried Fuchs <rhonda@debian.at>  Fri, 30 May 2008 08:34:31 +0200

netris (0.52-6) unstable; urgency=low

  * #include <time.h> in curses.c (closes: #345305)
  * Add patch to display line count, contributed by Piotr Krukowiecki
    (closes: #304224)
  * Bumped Standards-Version to 3.7.2, no changes needed.
  * Updated FSF address in copyright file.

 -- Gerfried Fuchs <alfie@debian.org>  Fri, 08 Sep 2006 14:08:42 -0500

netris (0.52-5) unstable; urgency=low

  * Erm, add small fix for 64bit machines from #325926 which was meant to be
    in the former upload already....  (closes: #325926)

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 05 Sep 2005 15:08:18 +0200

netris (0.52-4) unstable; urgency=low

  * Bumped standards version, no changes needed.
  * Moved menu file from /usr/lib/menu to /usr/share/menu.

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 05 Sep 2005 14:22:50 +0200

netris (0.52-3) unstable; urgency=low

  * Quote all entries in the menu file.
  * Fix -s setting, patch from Piotr Krukowiecki, thanks (closes: #300125)
  * Fixed short description to not include an upercased article at the start.
  * Slightly reformated copyright file a bit.

 -- Gerfried Fuchs <alfie@debian.org>  Fri, 18 Mar 2005 18:04:22 +0100

netris (0.52-2) unstable; urgency=low

  * Applied patch from Per von Zweigbergk for staircase effect (closes: #83039)
  * Bumped to policy 3.6.1: No changes needed.

 -- Gerfried Fuchs <alfie@debian.org>  Thu, 25 Nov 2004 14:24:01 +0100

netris (0.52-1) unstable; urgency=high

  * New upstream release which fixes buffer overflow vulnerability
    CAN-2003-0685 (closes: #205113) -- no other changes.
  * Updated to policy 3.6.0: No changes needed.

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 18 Aug 2003 21:25:09 +0200

netris (0.5-7) unstable; urgency=low

  * Added 'n'ew game key to -k handling option (updated manual page wrt/
    this).  Don't know though how to make the correct key show up in the
    message so simply changed it.

 -- Gerfried Fuchs <alfie@debian.org>  Fri, 18 Oct 2002 19:35:13 +0200

netris (0.5-6) unstable; urgency=low

  * Applied multi game patch with scoring from Tomas Berndtsson, received via
    private mail.
  * Removed /usr/doc -> /usr/share/doc handling.
  * Removed some superfluous commas from the long description.

 -- Gerfried Fuchs <alfie@debian.org>  Thu, 26 Sep 2002 22:47:20 +0200

netris (0.5-5) unstable; urgency=low

  * Updated watchfile to uscan version=2.
  * Updated to policy 3.5.7: Added support for DEB_BUILD_OPTIONS too.
  * Remove Makefile, .depend and config.h in clean target, too.

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 09 Sep 2002 18:19:59 +0200

netris (0.5-4) unstable; urgency=medium

  * wrote man page for netris-sample-robot, finally (closes: #19373)
  * un-debhelper-ized the package.
  * urgency=medium for having the libncurses4 removed from woody (previous
    upload forgot to set urgency)

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 18 Feb 2002 12:59:56 +0100

netris (0.5-3) unstable; urgency=low

  * New Maintainer.
  * Rebuild against libncurses5 (closes: #93943)
  * Added manual page (thanks to Edward Betts for writing it) -- this doesn't
    close 19373 though, netris-sample-robot still has no manual page.
  * Removed emacs-junk from the end of this file.
  * Updated to standards version 3.5.6:
    * Fixed the pointer to the GPL in the copyright file.
    * Added Build-Depends: debhelper, libncurses5-dev to control file.
    * Binaries are stripped (closes: #127381)
  * Added watch file.

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 11 Feb 2002 18:43:49 +0100

netris (0.5-2) unstable; urgency=low

  * Change maintainer address
  * FHS compliant
  * Standards: 3.0.1

 -- Gergely Madarasz <gorgo@sztaki.hu>  Tue,  7 Sep 1999 21:17:37 +0200

netris (0.5-1) unstable; urgency=low

  * Compile with libncurses4
  * Update menu file and standards version
  * New upstream version

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Thu,  3 Jun 1999 15:19:38 +0200

netris (0.4-3) unstable; urgency=low

  * Fix spelling mistake in extended description (Closes: #18922)
  * Fix most lintian errors and warnings
  * Switch to debhelper

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Tue, 10 Mar 1998 22:32:59 +0100

netris (0.4-2) unstable; urgency=low

  * move sr.c.gz to /usr/doc/netris/examples (#16920)

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Sun, 11 Jan 1998 00:55:16 +0100

netris (0.4-1) unstable; urgency=low

  * Changed Configure to use ncurses
  * Initial Release.

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Wed,  6 Aug 1997 22:10:42 +0200
